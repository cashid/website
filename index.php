<html>
	<head>
		<title> CashID Website </title>
		<link rel="shortcut icon" type="image/png" href="/favicon.png"/>
		<link rel="stylesheet" href="css/brand.css">
	</head>
	<body style='display: flex; align-items: center; justify-content: center; text-align: center; width: 25rem; margin: auto;'>
		<div>
			<span style='display: block; font-size: xx-large; font-weight: bold;'>CashID</span>
			<span style='display: block; color: rgba(0, 0, 0, 0.66);'>authentication protocol</span>
			<img src='logo.png' alt='CashID logotype' style='display: block; width: 25rem; margin: 1rem;' />
			<span style='display: block;'>Read the specification on <a href='https://gitlab.com/cashid/protocol-specification' style='text-decoration: none; font-weight: bold; color black !important;'>&nbsp;<img src='gitlab.png' alt='Gitlab logotype' style='height: 1rem; width: 1rem; position: relative; top: 0.15rem;' />&nbsp;Gitlab</a>.</span>
			<span style='display: block;'>Download the <a href='https://cashid.info/files/cashid_beta_20190206.apk' style='text-decoration: none; font-weight: bold; color black !important;'>identity-manager for android</a>.</span>
		</div>
	<body>
</html>
